// SPDX-FileCopyrightText: Red Hat, Inc.
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once
#include <float.h>

#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gsk/gl/gskglrenderer.h>
#include <libmks.h>

G_BEGIN_DECLS

#define SNOWGLOBE_TYPE_WINDOW (snowglobe_window_get_type ())

G_DECLARE_FINAL_TYPE (SnowglobeWindow, snowglobe_window, SNOWGLOBE, WINDOW, GtkApplicationWindow)

void snowglobe_window_set_display_view  (SnowglobeWindow *self,
                                         GDBusConnection *connection);
void snowglobe_window_set_status_view   (SnowglobeWindow *self,
                                         const gchar     *icon_name,
                                         const gchar     *head,
                                         const gchar     *body);
void snowglobe_window_set_loading_view  (SnowglobeWindow *self);

SnowglobeWindow *
snowglobe_window_new (GtkApplication *app,
                      gboolean        is_fullscreen,
                      gboolean        is_decorated,
                      gboolean        emulate_touch,
                      gboolean        auto_resize,
                      gboolean        show_host_cursor);
G_END_DECLS
