// SPDX-FileCopyrightText: Red Hat, Inc.
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <glib/gi18n.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SNOWGLOBE_TYPE_APPLICATION (snowglobe_application_get_type ())

G_DECLARE_FINAL_TYPE (SnowglobeApplication, snowglobe_application, SNOWGLOBE, APPLICATION, GtkApplication)

SnowglobeApplication *snowglobe_application_new (const char        *application_id,
                                                 GApplicationFlags flags);
G_END_DECLS
