// SPDX-FileCopyrightText: Red Hat, Inc.
// SPDX-License-Identifier: GPL-3.0-or-later

#include "config.h"

#include <glib/gi18n.h>
#include <libmks.h>

#include "application.h"

int
main (int   argc,
      char *argv[])
{
  g_autoptr (SnowglobeApplication) app = NULL;
  int ret;
  mks_init ();

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  app = snowglobe_application_new (APP_ID, G_APPLICATION_HANDLES_COMMAND_LINE);
  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}
