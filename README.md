# Snowglobe

<img src="https://gitlab.gnome.org/bilelmoussaoui/snowglobe/-/raw/main/data/icons/hicolor/scalable/apps/com.belmoussaoui.snowglobe.svg" width="128px" height="128px" />

Virtualization viewer using QEMU over DBus

## Screenshots

<div align="center">
    <img src="./data/screenshots/screenshot1.png" />
</div>

## Building

### Dependencies

- `gtk4` >= 4.12 (required by libmks)
- `glib` >= 2.76 (required by libmks)
- `libmks` >= 0.1.2 (can be built as a meson subproject)
- `desktop-file-utils`
- `meson`

The application can be built with the classic meson build commands

```shell
meson _build --prefix=/usr -Dlibmks:introspection=disabled
ninja -C _build
ninja -C _build install
```

A [Flatpak manifest](./com.belmoussaoui.snowglobe.json) is included in the repository and can be used to build and run the application.

## Running

Once the application is installed, it can be run using `snowglobe`. Every argument that is passed to it is assumed
to be part of the QEMU subprocess you would like to execute. Otherwise you can run QEMU as a separate process yourself if you need more control.

```shell
snowglobe "qemu-system-x86_64 -enable-kvm -cpu host -device virtio-vga-gl,xres=1920,yres=1080 -m 8G -smp 4 -display dbus,gl=on -cdrom $HOME/Downloads/Fedora-Workstation.iso -hda $HOME/Downloads/fedora.img -boot d"
```

**Note** the `display` must be set to `dbus` otherwise things would fail badly.

You can also run QEMU yourself and Snowglobe as two separate processes.


```shell
qemu-system-x86_64 -enable-kvm -cpu host -device virtio-vga-gl,xres=1920,yres=1080 -m 8G -smp 4 -display dbus,gl=on -cdrom $HOME/Downloads/Fedora-Workstation.iso -hda $HOME/Downloads/fedora.img -boot d
```
and then run Snowglobe 

### Flatpak

When running inside a Flatpak container:

```shell
flatpak run com.belmoussaoui.snowglobe "qemu-system-x86_64 -enable-kvm -cpu host -device virtio-vga-gl,xres=1920,yres=1080 -m 8G -smp 4 -display dbus,gl=on -cdrom $HOME/Downloads/Fedora-Workstation.iso -hda $HOME/Downloads/fedora.img -boot d"
```

## Options

```shell
Help Options:
  -h, --help                 Show help options
  --help-all                 Show all help options
  --help-gapplication        Show GApplication options

Application Options:
  --undecorate               Disable window decoration
  --fullscreen               Start the window fullscreened
  --emulate-touch            Emulate touch from mouse motion and click events
  --show-host-cursor         Display host cursor
  --auto-resize              Enable auto reconfiguring the guest OS's display size when resized
```
